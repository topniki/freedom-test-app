export interface AuthLoginParams {
  login: string;
  password: string;
}

export interface User {
  login: string;
  username?: string;
}
