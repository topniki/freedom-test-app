export interface Document {
  id: string;
  header: string;
  content: string;
  comment?: string;
  approver?: string;
  resolution?: DocumentResolution;
  state?: DocumentState;
}

export type DocumentResolution = 'approved' | 'completely approved' | 'not approved' | 'blue color approved';
export type DocumentState = 0 | 1;

export interface ResolutionStruct {
  name: string;
  resolution: DocumentResolution;
};


export const RESOLUTIONS: Array<ResolutionStruct> = [
  {
    name: 'Полностью согласен',
    resolution: 'completely approved'
  },
  {
    name: 'Согласен',
    resolution: 'approved'
  },
  {
    name: 'Не согласен',
    resolution: 'not approved'
  },
  {
    name: 'Разрешаю карсить в синий цвет',
    resolution: 'blue color approved'
  },
];

export interface DocumentApproveState {
  approver: string;
  resolution: DocumentResolution;
  comment: string;
  state: DocumentState;
}
