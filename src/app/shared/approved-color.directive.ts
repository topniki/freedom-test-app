import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { DocumentResolution, DocumentState } from './models';

@Directive({
  selector: '[appApprovedColor]'
})
export class ApprovedColorDirective implements OnInit {
  @Input('appApprovedColor') state: DocumentResolution | DocumentState | undefined;

  color: 'red' | 'green';

  constructor(private el: ElementRef) { }

  ngOnInit(): void {
    if (typeof this.state === 'string') {
      this.color = this.state === 'not approved' ? 'red' : 'green';
    } else if (this.state || this.state === 0) {
      this.color = this.state ? 'green' : 'red';
    }

    if (this.color) {
      this.el.nativeElement.style.color = this.color;
    }
  }

}
