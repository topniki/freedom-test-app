import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { ApprovedColorDirective } from './approved-color.directive';


const MATERIALS = [
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatSelectModule
];


@NgModule({
  declarations: [ApprovedColorDirective],
  imports: [
    CommonModule,
    ...MATERIALS,
  ],
  exports: [
    ...MATERIALS,
    ApprovedColorDirective
  ]
})
export class SharedModule { }
