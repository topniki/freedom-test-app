import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Document, User } from '../shared/models';

@Injectable({
  providedIn: 'root'
})
export class AppStateService {

  private currentUser$: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(null);
  private documents$: BehaviorSubject<Document[] | null> = new BehaviorSubject<Document[] | null>(null);

  constructor() { }

  setCurrentUser(user: User): void {
    this.currentUser$.next(user);
  }

  getCurrentUser(): Observable<User | null> {
    return this.currentUser$.asObservable();
  }


  setDocuments(documents: Document[]): void {
    this.documents$.next(documents);
  }

  getDocuments(): Observable<Document[] | null> {
    return this.documents$.asObservable();
  }

  getDocumentById(id: string): Observable<Document | null> {
    return this.documents$.asObservable().pipe(
      map(documents => {
        return documents ? documents.find(d => d.id === id) || null : null;
      })
    );
  }

  approveDocument(documentId: string, newDoc: Document): void {
    const documents = [...this.documents$.value || []];
    const oldDoc = documents.find(d => d.id === documentId);

    Object.assign(oldDoc, newDoc);

    this.setDocuments(documents);
  }
}
