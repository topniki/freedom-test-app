import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { AuthLoginParams, User } from '../shared/models';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(loginParams: AuthLoginParams): Observable<User> {
    return this.getAllUsers().pipe(
      mergeMap(users => {
        const user = users.find(u => u.login === loginParams.login);

        return user ? of(user) : throwError(new HttpErrorResponse({
          error: new Error('user not found'),
          status: 404
        }));
      })
    );
  }

  private getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>('assets/json/users.json');
  }
}
