import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { AppStateService } from '../core/app-state.service';
import { BaseComponent } from '../shared/base.class';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent extends BaseComponent implements OnInit {

  form: FormGroup;
  showPassword = false;
  errorMessage: string | null = null;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private stateService: AppStateService) {
    super();
    this.form = this.fb.group({
      login: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.form.valueChanges
    .pipe(takeUntil(this.destroy$))
    .subscribe(() => this.errorMessage = null);
  }

  submit(): void {
    this.authService.login(this.form.getRawValue())
    .subscribe(
      user => {
        this.stateService.setCurrentUser(user);
        this.router.navigate(['']);
      },
      (error: HttpErrorResponse) => this.errorMessage = (error.error as Error).message
    );
  }
}
