import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { Document, DocumentApproveState } from '../shared/models';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {

  constructor(private http: HttpClient) { }

  getAllDocuments(): Observable<Document[]> {
    return this.http.get<Document[]>('assets/json/documents.json');
  }

  approveDocument(documentId: string, approveState: DocumentApproveState): Observable<Document> {
    return this.getDocumentById(documentId).pipe(
      map(document => {
        return { ...document, ...approveState };
      })
    );
  }

  private getDocumentById(documentId: string): Observable<Document> {
    return this.getAllDocuments().pipe(
      mergeMap(documents => {
        const document = documents.find(d => d.id === documentId);

        return document ? of(document) : throwError(new HttpErrorResponse({
          error: new Error('document not found'),
          status: 404
        }));
      })
    );
  }
}
