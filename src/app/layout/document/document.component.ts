import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { first, mergeMap, tap } from 'rxjs/operators';

import { AppStateService } from 'src/app/core/app-state.service';
import { DocumentApproveState, DocumentState, RESOLUTIONS, ResolutionStruct } from 'src/app/shared/models';
import { Document } from '../../shared/models';
import { LayoutService } from '../layout.service';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocumentComponent implements OnInit {
  @Input() documentId: string;

  document$: Observable<Document | null>;
  form: FormGroup;

  readonly resolutions: Array<ResolutionStruct> = RESOLUTIONS;

  constructor(private fb: FormBuilder,
              private stateService: AppStateService,
              private layoutService: LayoutService) {}

  ngOnInit(): void {
    this.initForm();
    this.document$ = this.stateService.getDocumentById(this.documentId).pipe(
      tap(doc => this.updateFormFromDocument(doc))
    );
  }

  approve(documentId: string, state: DocumentState): void {
    this.stateService.getCurrentUser().pipe(
      first(user => !!user),
      mergeMap(user => {
        const approvedState: DocumentApproveState = {
          ...this.form.getRawValue(),
          approver: user?.login,
          state
        };
        return this.layoutService.approveDocument(documentId, approvedState);
      })
    ).subscribe(document => {
      this.stateService.approveDocument(documentId, document);
    });
  }

  private initForm(): void {
    this.form = this.fb.group({
      comment: [''],
      resolution: ['', [Validators.required]]
    });
  }

  private updateFormFromDocument(document: Document | null): void {
    this.form.patchValue({
      comment: document?.comment,
      resolution: document?.resolution,
    });

    this.form.updateValueAndValidity();
  }

}
