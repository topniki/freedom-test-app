import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Document, RESOLUTIONS } from '../../../shared/models';

@Component({
  selector: 'app-resolution-info',
  templateUrl: './resolution-info.component.html',
  styleUrls: ['./resolution-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResolutionInfoComponent implements OnInit {
  @Input() document: Document;

  documentResolutionName: string;

  ngOnInit(): void {
    this.documentResolutionName = RESOLUTIONS.find(r => r.resolution === this.document.resolution)?.name || '';
  }

}
