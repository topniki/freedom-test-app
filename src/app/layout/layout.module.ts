import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { DocumentComponent } from './document/document.component';
import { SharedModule } from '../shared/shared.module';
import { ResolutionInfoComponent } from './document/resolution-info/resolution-info.component';

@NgModule({
  declarations: [LayoutComponent, DocumentComponent, ResolutionInfoComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class LayoutModule { }
