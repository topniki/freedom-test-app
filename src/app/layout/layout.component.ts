import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { AppStateService } from '../core/app-state.service';
import { Document } from '../shared/models';
import { LayoutService } from './layout.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutComponent {

  documents$: Observable<Document[] | null> = this.stateService.getDocuments();

  constructor(private layoutService: LayoutService,
              private stateService: AppStateService) {
    this.layoutService.getAllDocuments().subscribe(
      documents => this.stateService.setDocuments(documents)
    );
  }
}
